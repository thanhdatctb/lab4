﻿using Lab4.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4
{
    public partial class Form1 : Form
    {
        private StudentContextDB db = new StudentContextDB();
        public Form1()
        {
            InitializeComponent();
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        public void SetGridViewStyle(DataGridView dgview)
        {
            dgview.BorderStyle = BorderStyle.None;
            dgview.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239,
           249);
            dgview.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dgview.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dgview.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dgview.BackgroundColor = Color.White;
            dgview.EnableHeadersVisualStyles = false;
            dgview.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dgview.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            dgview.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dgview.AllowUserToDeleteRows = false;
            dgview.AllowUserToAddRows = false;
            dgview.AllowUserToOrderColumns = true;
            dgview.MultiSelect = false;
            dgview.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                SetGridViewStyle(dtgvStudent);
                StudentContextDB context = new StudentContextDB();
                List<Faculty> listFalcultys = context.Faculties.ToList();
                List<Student> listStudent = context.Students.ToList();
                FillFalcultyCombobox(listFalcultys);
                BindGrid(listStudent);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void FillFalcultyCombobox(List<Faculty> listFalcultys)
        {
            this.cmbFaculty.DataSource = listFalcultys;
            this.cmbFaculty.DisplayMember = "FacultyName";
            this.cmbFaculty.ValueMember = "FacultyID";
        }

        private void loadData()
        {
            var list = db.Students.ToList();
            BindGrid(list);
        }
        private void BindGrid(List<Student> listStudent) 
        {
             dtgvStudent.Rows.Clear();
             foreach (var item in listStudent)
             {
                 int index = dtgvStudent.Rows.Add();
                 dtgvStudent.Rows[index].Cells[0].Value = item.StudentID;
                 dtgvStudent.Rows[index].Cells[1].Value = item.FullName;
                 if(item.Faculty!= null)
                 dtgvStudent.Rows[index].Cells[2].Value = item.Faculty.FacultyName;
                 dtgvStudent.Rows[index].Cells[3].Value = item.AverageScore + "";
             }
        }

        private void AddUpdate_Click(object sender, EventArgs e)
        {
            string id = this.txtId.Text;
            string name = this.txtName.Text;
            double average = double.Parse(this.txtAverageScore.Text);
            int FacId = int.Parse(this.cmbFaculty.SelectedValue.ToString());
            Student student = new Student();
            student.StudentID = id;
            student.FullName = name;
            student.AverageScore = average;
            student.FacultyID = FacId;
            if(db.Students.Find(id) == null)
            {
                try
                {
                    db.Students.Add(student);
                    db.SaveChanges();
                    loadData();
                    MessageBox.Show("Thêm sinh viên thành công");
                }catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                
            }
            else
            {

                try
                {
                    Student dbUpdate = db.Students.FirstOrDefault(p => p.StudentID == this.txtId.Text);
                    if (dbUpdate != null)
                    {
                        dbUpdate.StudentID = id;
                        dbUpdate.FullName = name;
                        dbUpdate.AverageScore = average;
                        dbUpdate.FacultyID = FacId;
                        db.SaveChanges(); //lưu thay đổi
                        db.SaveChanges();
                        loadData();
                        MessageBox.Show("Sửa sinh viên thành công");
                    }

                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            } 
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Bạn có chắc chắn muốn xóa", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (result.Equals(DialogResult.OK))
            {
                //Do something
                try
                {
                    Student std = db.Students.Find(this.txtId.Text);
                    db.Students.Remove(std);
                    db.SaveChanges();
                    MessageBox.Show("Xóa thành công");
                    loadData();
                }catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
            }

        }
    }
  
}

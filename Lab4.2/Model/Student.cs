namespace Lab4.Model
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    public partial class Student
    {
       
        public string StudentID { get; set; }

     
        public string FullName { get; set; }

        public double? AverageScore { get; set; }

        public int? FacultyID { get; set; }

        public virtual Faculty Faculty { get; set; }
        public  List<Student> GetAllStudent()
        {
            List<Student> listStudent = new List<Student>();
            string connectionString =
           ConfigurationManager.ConnectionStrings["DSStudentConnectString"].ConnectionString;
            string queryString = "Select * from Student";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                command.CommandType = CommandType.Text;
                connection.Open();
                using (SqlDataReader objReader = command.ExecuteReader())
                {
                    if (objReader.HasRows)
                    {
                        while (objReader.Read())
                        {
                            Student temp = new Student();
                            string studentID =
                           objReader.GetString(objReader.GetOrdinal("StudentID"));
                            if (studentID != null)
                            {
                                temp.StudentID = studentID;
                                temp.FullName = objReader["FullName"].ToString();
                                string averageScore =
                               objReader["AverageScore"].ToString();
                                if (averageScore != null)
                                    temp.AverageScore =
                                   Convert.ToDouble(averageScore);
                                string falcutyID = objReader["FacultyID"].ToString();
                                if (falcutyID != null)
                                    temp.FacultyID = Convert.ToInt32(falcutyID);
                            }
                            listStudent.Add(temp);
                        }
                    }
                }

            }
            return listStudent;
        }
        public void ExcuteNonQuery(string commandText)
        {
            string connectionString =
            ConfigurationManager.ConnectionStrings["DSStudentConnectString"].ConnectionString;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(commandText, connection);
                command.CommandType = CommandType.Text;
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public void InsertStudent()
        {
            string queryString = @"INSERT INTO Student
             ([StudentID]
             ,[FullName]
             ,[AverageScore]
             ,[FacultyID])
             VALUES('{0}' ,'{1}',{2},{3})";
            queryString = string.Format(queryString, this.StudentID, this.FullName,
           this.AverageScore, this.FacultyID);
            ExcuteNonQuery(queryString);
        }
        public void UpdateStudent(string studentUpdateID)
        {
            string queryString = @"UPDATE Student
         Set [FullName] = '{1}'
         ,[AverageScore] = '{2}'
         ,[FacultyID] = '{3}'WHERE StudentID= '{0}'";
            queryString = string.Format(queryString, this.StudentID, this.FullName,
           this.AverageScore, this.FacultyID);
            ExcuteNonQuery(queryString);
        }
        public void DeleteStudent()
        {
            string queryString = @"DELETE FROM Student
             WHERE StudentID= '{0}'";
            queryString = string.Format(queryString, this.StudentID);
            ExcuteNonQuery(queryString);
        }

    }
}

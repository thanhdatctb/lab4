namespace Lab4.Model
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    public partial class Faculty
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Faculty()
        {
            Students = new HashSet<Student>();
        }

        
        public int FacultyID { get; set; }

        
        public string FacultyName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Student> Students { get; set; }
        public List<Faculty> GetAll()
        {
            List<Faculty> faculties = new List<Faculty>();
            string connectionString =
           ConfigurationManager.ConnectionStrings["DSStudentConnectString"].ConnectionString;
            string queryString = "Select * from Faculty";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                command.CommandType = CommandType.Text;
                connection.Open();
                using (SqlDataReader objReader = command.ExecuteReader())
                {
                    if (objReader.HasRows)
                    {
                        while (objReader.Read())
                        {
                            Faculty temp = new Faculty();
                            int id = objReader.GetInt32(objReader.GetOrdinal("FacultyID"));
                            if (id != null)
                            {
                                temp.FacultyID = id;
                                temp.FacultyName = objReader["FacultyName"].ToString();
                               
                                
                            }
                            faculties.Add(temp);
                        }
                    }
                }

            }
            return faculties;
        }
    }
}
